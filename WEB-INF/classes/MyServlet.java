import javax.servlet.http.*;
import javax.servlet.ServletException;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns={"/hello"})
public class MyServlet extends HttpServlet {

	public void service(HttpServletRequest req, HttpServletResponse res) 
	throws ServletException
	{
		try {
			res.getWriter().println("Hello world 4 !");
		} catch( IOException ioe ) {
			throw new ServletException( ioe );
		}

	}

}





